/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.walab.DO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gsilva
 */
@Entity
@Table(name = "Pilot")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pilot.findAll", query = "SELECT p FROM Pilot p"),
    @NamedQuery(name = "Pilot.findById", query = "SELECT p FROM Pilot p WHERE p.id = :id"),
    @NamedQuery(name = "Pilot.findByName", query = "SELECT p FROM Pilot p WHERE p.name = :name"),
    @NamedQuery(name = "Pilot.findByLastName", query = "SELECT p FROM Pilot p WHERE p.lastName = :lastName"),
    @NamedQuery(name = "Pilot.findByLicense", query = "SELECT p FROM Pilot p WHERE p.license = :license"),
    @NamedQuery(name = "Pilot.findByCity", query = "SELECT p FROM Pilot p WHERE p.city = :city"),
    @NamedQuery(name = "Pilot.findByState", query = "SELECT p FROM Pilot p WHERE p.state = :state")})
public class Pilot implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "lastName")
    private String lastName;
    @Size(max = 2)
    @Column(name = "license")
    private String license;
    @Size(max = 50)
    @Column(name = "city")
    private String city;
    @Column(name = "state")
    private Boolean state;

    public Pilot() {
    }

    public Pilot(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pilot)) {
            return false;
        }
        Pilot other = (Pilot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.walab.DO.Pilot[ id=" + id + " ]";
    }
    
}
