package com.mycompany.walab.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 *
 * @author 
 */
@Path("javaee8")
public class JavaEE8Resource {
    
    @GET
    public Response ping(){
        return Response
                .ok("ping")
                .build();
    }
    
    
    @GET
    @Path("ping2")
    public Response ping2(){
        return Response
                .ok("ping")
                .build();
    }
}
