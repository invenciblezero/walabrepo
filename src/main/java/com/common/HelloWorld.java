/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.common;

/**
 *
 * @author reload
 */

import java.io.Serializable;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@ViewScoped
@Named("pc_helloWorld")
public class HelloWorld implements Serializable{

	private static final long serialVersionUID = 1L;
	private String bio;
	
	public String getBio() {
		return bio;
	}
	
	public void setBio(String bio) {
		this.bio = bio;
	}
}
